package handler

import (
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
)

func (h *Handler) RunDriver() error {
	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	// добавляем в конфигурацию драйвера настройки для chrome
	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)

	// переменная нашего веб драйвера
	var err error
	// прописываем адрес нашего драйвера
	urlPrefix := "http://chrome-selenium:4444/wd/hub"

	h.driver, err = selenium.NewRemote(caps, urlPrefix)
	if err != nil {
		return err
	}
	return nil
}

func (h *Handler) QuitServer() error {
	return h.driver.Quit()
}
