package handler

import (
	"fmt"

	"net/http"
	"strconv"
	"strings"

	"gitlab.com/timofeev.pavel.art/vacancy-app/repository"

	"github.com/go-chi/chi/v5/middleware"

	"github.com/PuerkitoBio/goquery"
	"github.com/go-chi/chi/v5"
	"github.com/tebeka/selenium"
)

const HabrCareerLink = "https://career.habr.com"

type Handler struct {
	storage repository.VacancyStorager
	driver  selenium.WebDriver
}

func NewHandler(wd selenium.WebDriver) *Handler {
	return &Handler{
		driver:  wd,
		storage: repository.NewVacancyStorage(),
	}
}

func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./doc"))).ServeHTTP(w, r)
	})

	r.Post("/search", h.VacancySearch)
	r.Delete("/delete", h.VacancyDelete)
	r.Get("/get", h.VacancyGetByID)
	r.Get("/list", h.VacancyGetList)

	return r
}

// VacancyCount считает общее кол-во вакансий по запросу
func (h *Handler) VacancyCount() (int, error) {
	elem, err := h.driver.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		return 0, err
	}
	vacancyCountRaw, err := elem.Text()
	if err != nil {
		return 0, err
	}

	vacancyCount, err := strconv.Atoi(strings.Split(vacancyCountRaw, " ")[1])
	if err != nil {
		return 0, err
	}
	return vacancyCount, nil
}

// VacancyLinkList выводит список ссылок текущей страницы
func (h *Handler) VacancyLinkList() ([]string, error) {
	elems, err := h.driver.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
	if err != nil {
		return nil, err
	}
	var links []string
	for _, elem := range elems {
		var link string
		link, err = elem.GetAttribute("href")
		if err != nil {
			return nil, err
		}
		links = append(links, HabrCareerLink+link)
	}

	return links, nil
}

// GetPage получение содержания страницы в JSON формате
func (h *Handler) GetPage(link string) ([]byte, error) {
	resp, err := http.Get(link)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode > 299 {
		return nil, fmt.Errorf(link + " page is not available")
	}

	var doc *goquery.Document
	doc, err = goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return nil, err
	}
	dd := doc.Find("script[type=\"application/ld+json\"]")
	if dd == nil {
		return nil, fmt.Errorf("habr vacancy nodes not found")
	}
	ss := dd.First().Text()
	return []byte(ss), nil
}
