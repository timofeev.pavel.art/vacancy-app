package handler

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/timofeev.pavel.art/vacancy-app/models"
)

// swagger:route POST /search vacancy vacancySearchRequest
// Search and write into db info about vacancy by query
// responses:
//	200: vacancySearchResponse
//	400: description: Bad Request
//	500: description: Internal Server Error

func (h *Handler) VacancySearch(w http.ResponseWriter, r *http.Request) {
	var (
		err   error
		count int
		links []string
		page  []byte
	)
	query := r.URL.Query()["query"]

	status, err := h.driver.Status()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if status.Ready {
		err = h.RunDriver()
		if err != nil {
			log.Fatal(err)
		}
	}

	if err = h.driver.Get(HabrCareerLink + fmt.Sprintf("/vacancies?page=%d&q=%s&type=all", 1, query)); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	count, err = h.VacancyCount()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if count%25 != 0 {
		count = count/25 + 1
	} else {
		count = count / 25
	}

	for i := 1; i != count+1; i++ {
		var list []string
		if err = h.driver.Get(HabrCareerLink + fmt.Sprintf("/vacancies?page=%d&q=%s&type=all", i, query)); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		list, err = h.VacancyLinkList()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		links = append(links, list...)
	}

	err = h.QuitServer()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	output := make(map[string]int, 1)
	output["was recorded:"] = 0
	for _, link := range links {
		var vacancy models.Vacancy

		page, err = h.GetPage(link)
		if err != nil {
			log.Println(err)
			continue
		}
		if string(page) == "" {
			continue
		}
		vacancy.Link = link

		if err = vacancy.Unmarshal(page); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if err = h.storage.Create(vacancy); err != nil {
			log.Println(link, err)
		}
		output["was recorded:"]++
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err = json.NewEncoder(w).Encode(output); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// swagger:route DELETE /delete vacancy vacancyDeleteRequest
// Get vacancy by ID
// responses:
//	200: vacancyDeleteResponse
//	400: description: Bad Request
//	404: description: Not Found

func (h *Handler) VacancyDelete(w http.ResponseWriter, r *http.Request) {
	var (
		err      error
		vacIDRaw string
		vacID    int
	)

	vacIDRaw = r.URL.Query()["id"][0]
	vacID, err = strconv.Atoi(vacIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err = h.storage.Delete(vacID); err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}

// swagger:route GET /get vacancy vacancyGetRequest
// Get vacancy by ID
// responses:
//	200: vacancyGetResponse
//	400: description: Bad Request
//	404: description: Not Found
//	500: description: Internal Server Error

func (h *Handler) VacancyGetByID(w http.ResponseWriter, r *http.Request) {
	var (
		vacancy  models.Vacancy
		err      error
		vacIDRaw string
		vacID    int
	)

	vacIDRaw = r.URL.Query()["id"][0]
	vacID, err = strconv.Atoi(vacIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	vacancy, err = h.storage.GetByID(vacID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err = json.NewEncoder(w).Encode(vacancy); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// swagger:route GET /list vacancy vacancyListRequest
// Display all stored vacancy
// responses:
//	200: description: vacancyListResponse
// 	404: description: List is empty
//	500: description: Internal Server Error

func (h *Handler) VacancyGetList(w http.ResponseWriter, r *http.Request) {
	list, err := h.storage.GetList()
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err = json.NewEncoder(w).Encode(list); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
