// nolint:all
package handler

// swagger:parameters vacancySearchRequest
type vacancySearchRequest struct {
	// In:query
	Query string `json:"query"`
}

// swagger:parameters vacancyDeleteRequest
type vacancyDeleteRequest struct {
	// ID of vacancy to delete
	//
	// in: query
	ID string `json:"id"`
}

// swagger:parameters vacancyGetRequest
type vacancyGetRequest struct {
	// ID of vacancy to get
	//
	// in: query
	ID string `json:"id"`
}

// swagger:parameters vacancyListRequest
type vacancyListRequest struct {
}
