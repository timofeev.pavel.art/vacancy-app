package models

import (
	"encoding/json"
	"strconv"
)

func (t *Vacancy) Unmarshal(data []byte) error {
	var parse RawVacancy

	err := json.Unmarshal(data, &parse)
	if err != nil {
		return err
	}
	t.Title = parse.Title
	t.Identifier, err = strconv.Atoi(parse.Identifier.Value)
	if err != nil {
		return err
	}
	t.Company = parse.HiringOrganization.Name
	t.Site = parse.HiringOrganization.SameAs
	t.Description = parse.Description
	t.DatePost = parse.DatePosted
	return nil
}

// Vacancy store info about vacancy
type Vacancy struct {
	ID          int    `json:"id"`
	Identifier  int    `json:"identifier"`
	Link        string `json:"link"`
	Title       string `json:"title"`
	Company     string `json:"company"`
	Site        string `json:"site"`
	Description string `json:"description"`
	DatePost    string `json:"datePost"`
}

// RawVacancy struct for parsing info about vacancy json
type RawVacancy struct {
	Title              string             `json:"title"`
	Description        string             `json:"description"`
	Identifier         Identifier         `json:"identifier"`
	HiringOrganization HiringOrganization `json:"hiringOrganization"`
	DatePosted         string             `json:"datePosted"`
}

type Identifier struct {
	Value string `json:"value"`
}

// HiringOrganization struct for parsing info about organization
type HiringOrganization struct {
	Name   string `json:"name"`
	SameAs string `json:"sameAs"`
}
