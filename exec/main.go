package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"time"

	"gitlab.com/timofeev.pavel.art/vacancy-app"
	"gitlab.com/timofeev.pavel.art/vacancy-app/handler"

	"github.com/tebeka/selenium"
)

const port = ":8080"

func main() {
	var (
		err    error
		driver selenium.WebDriver
		server vacancy.Server
	)

	handle := handler.NewHandler(driver)
	if err = handle.RunDriver(); err != nil {
		log.Fatal(err)
	}

	go func() {
		if err = server.Run(handle.Routes(), port); err != nil {
			if err = handle.QuitServer(); err != nil {
				log.Fatal(err)
			}
			log.Fatal(err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err = handle.QuitServer(); err != nil {
		log.Fatal(err)
	}

	if err = server.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting.")
}
