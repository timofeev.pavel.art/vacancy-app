module gitlab.com/timofeev.pavel.art/vacancy-app

go 1.20

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/go-chi/chi/v5 v5.0.8
	github.com/tebeka/selenium v0.9.9
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	golang.org/x/net v0.7.0 // indirect
)
