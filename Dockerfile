# Build stage
FROM golang:1.20-alpine AS builder
RUN go version
ENV GOPATH=/
RUN mkdir /app
ADD . /app/
WORKDIR /app
COPY . .
RUN go mod download
RUN go build -o vacancy-app ./exec/main.go

# Run stage
FROM alpine
WORKDIR /app
ADD . /doc/
COPY --from=builder /app/doc/swagger.json /app/doc/
COPY --from=builder /app/vacancy-app .

CMD ["/app/vacancy-app"]