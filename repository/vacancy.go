package repository

import (
	"fmt"
	"sync"

	"gitlab.com/timofeev.pavel.art/vacancy-app/models"
)

type VacancyStorager interface {
	Create(dto models.Vacancy) error
	GetByID(id int) (models.Vacancy, error)
	GetList() ([]models.Vacancy, error)
	Delete(id int) error
}

type VacancyStorage struct {
	data               []*models.Vacancy
	primaryKeyIDx      map[int]*models.Vacancy
	autoIncrementCount int
	sync.Mutex
}

func NewVacancyStorage() *VacancyStorage {
	return &VacancyStorage{
		primaryKeyIDx:      make(map[int]*models.Vacancy, 100),
		autoIncrementCount: 1,
	}
}

func (vs *VacancyStorage) Create(vac models.Vacancy) error {
	vs.Lock()
	defer vs.Unlock()
	if _, ok := vs.primaryKeyIDx[vac.Identifier]; ok {
		return fmt.Errorf("already exist")
	}

	vac.ID = vs.autoIncrementCount
	vs.primaryKeyIDx[vac.Identifier] = &vac
	vs.autoIncrementCount++
	vs.data = append(vs.data, &vac)
	return nil
}

func (vs *VacancyStorage) GetByID(id int) (models.Vacancy, error) {
	vs.Lock()
	defer vs.Unlock()
	if v, ok := vs.primaryKeyIDx[id]; ok {
		return *v, nil
	}

	return models.Vacancy{}, fmt.Errorf("not found")
}

func (vs *VacancyStorage) GetList() ([]models.Vacancy, error) {
	vs.Lock()
	defer vs.Unlock()
	if len(vs.data) == 0 {
		return nil, fmt.Errorf("database is empty")
	}

	list := make([]models.Vacancy, len(vs.data))
	for i, item := range vs.data {
		list[i] = *item
	}

	return list, nil
}

func (vs *VacancyStorage) Delete(id int) error {
	vs.Lock()
	defer vs.Unlock()

	vac, ok := vs.primaryKeyIDx[id]
	if !ok {
		return fmt.Errorf("not found")
	}

	delete(vs.primaryKeyIDx, id)
	for i := range vs.data {
		if vac == vs.data[i] {
			switch {
			case len(vs.data) == 1:
				vs.data = []*models.Vacancy{}
				return nil
			case i == 0:
				vs.data = vs.data[i+1:]
				return nil
			case i == len(vs.data)-1:
				vs.data = vs.data[:i]
				return nil
			default:
				vs.data = append(vs.data[:i], vs.data[i+1:]...)
				return nil
			}
		}
	}

	return nil
}
